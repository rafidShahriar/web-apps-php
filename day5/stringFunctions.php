<?php
function newline(){
    echo '<br/>';
}

// php string functions
//PHP addslashes() Function
$str = addslashes('What does "YOLO" mean');
echo $str;
newline();

// PHP explode() Function
$str1 = "Hello World, It's a beautiful day.";
echo '<pre>';
print_r(explode(", ", $str1));
echo '</pre>';
newline();

// html entities
$str2 = "<a>";
echo htmlentities($str2);
newline();

// PHP implode() Function
$arr = array('Hello', 'World', ',', 'how', 'are', 'you');
echo implode(" ", $arr);
newline();

//PHP ltrim functions

$str3 = "Hello World";
echo $str3;
newline();
echo ltrim($str3, "Hello");
newline();

//nl2br function
echo nl2br("One line. \nanother line");
?>