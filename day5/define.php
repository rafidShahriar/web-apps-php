<?php
# assume this is a class file defines two functions : foo() and bar()
# location of the file phpstar/foobar.php
namespace phpstar;
class fooBar
{
    public function foo()
    {
	echo 'hello world, from function foo';
    }

    public function bar()
    {
	echo 'hello world, from function bar';
    }
}

echo __NAMESPACE__;

function newline(){
    echo '<br/>';
}

newline();

// define( ) function 
define('bitm', 'I am a student of Bitm' );
echo bitm."<br/>";

// line function
echo __LINE__."<br/>";

//file function
echo __FILE__;
newline();

// DIR function
echo __DIR__;
newline();

// FUNCTION function
function myName(){
    echo 'my name is PHP Artisan';
    echo '<br/>';
    echo __FUNCTION__;
}

myName();

newline();

// regular expression
$str1 = 'The quick brown fox';  
echo preg_replace('/\W\w+\s*(\W*)$/', '$1', $str1); 
newline();

// mathematical expression
$a = 2;
$b = 3;
$c = $a * $b;
echo $c;
newline();

// operator - exponentiations
//Result of raising $x to the $y'th power (Introduced in PHP 5.6)
//$d = $a**$b;




?>

