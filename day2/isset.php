<!-- isset - Determine if a variable is set and is not null -->
<?php

$var = '';

// this will evaluate to TRUE so the text will be printed.
if (isset($var)) {
	echo "This variable is set so that this line is printed.";
}

?>