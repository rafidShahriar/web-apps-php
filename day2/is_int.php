<!-- is_int - The is_int() function is used to test whether the type of the specified vaiable is an integer or not -->

<?php

$variable1 = 1.5;
$variable2 = 53;

if (is_int($variable2)) {
	echo "This is an int";
}
// else if (is_int($variable2)) {
// 	echo "This is an int";
// }
else {
	echo "This is not an int";
}

?>