<!-- is_array - The is_array() function is used to find whether a variable is an array or not -->

<?php

$var_name = array('Apple', 'Ball', 'Cat');

if (is_array($var_name)) {
	echo "This is an array.";
} else {
	echo "This is not an array.";
}


?>