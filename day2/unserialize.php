<!-- unserialize- The unserialize() converts to actual data from serialized data -->

<?php

// at first, serialize the data
$serialized_data = serialize(array('math', 'language', 'science'));
echo $serialized_data.'<br>';

// unserialize the data
$var1 = unserialize($serialized_data);

// show the unserialized data
var_dump($var1);

?>