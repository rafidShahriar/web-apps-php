<!-- is_null() - The is_null() function is used to test whether a variable is null or not -->

<?php

$variable = 'abc';

if (is_null($variable)) {
	echo "The variable is null";
} else {
	echo "The variable is not null";
}


?>