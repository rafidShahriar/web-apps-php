<!-- is_string() - The is_string function is used to find whether a variable is a string or not -->

<?php

$variable1 = 99;

$variable2 = '99';

if (is_string($variable1)) {
	echo "This is a sting";
} else {
	echo "This is not a string"."<br/>";
}

if (is_string($variable2)) {
	echo "This is a sting";
} else {
	echo "This is not a string";
}

echo "<pre>";

var_dump(is_string($variable1));

var_dump(is_string($variable2));

var_dump(is_string("768866"));

echo "</pre>";

?>