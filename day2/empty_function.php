<?php

$var = 0;

// evaluates to true because $var is empty
if (empty($var)) {
	echo "variable is either 0, empty or not set at all."."\n";
}

// evaluates as true because $var is set
if (empty($var)) {
	echo "Variable is set even though it is empty.";
}

?>