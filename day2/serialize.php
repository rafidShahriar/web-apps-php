<!-- serialize- Generates a storable representation of a value -->

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<?php

		$serialized_data = serialize(array('Math', 'Language', 'Science'));
		echo $serialized_data.'<br>';

	?>
</body>
</html>